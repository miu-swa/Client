const SERVICES_BASE_URL = import.meta.env.VITE_API_SERVICES_URL;

export const getRunningServices = async () => {
  const response = await fetch(`${SERVICES_BASE_URL}`);
  return response.json();
};
