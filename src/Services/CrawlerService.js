const CRAWLER_BASE_URL = import.meta.env.VITE_API_CRAWLER_URL;

export const fetchCrawlerData = async () => {
  const response = await fetch(`${CRAWLER_BASE_URL}`);
  const data = await response.json();
  return data;
};

export async function postCrawler(urlsString) {
  const response = await fetch(`${CRAWLER_BASE_URL}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(urlsString),
  });
  const res = await response.json();
  return res;
}
export async function deleteCrawler(url) {
  const response = await fetch(`${CRAWLER_BASE_URL}?url=${url}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  });
}
