// import { useApiContext } from "../context/ApiContext";
import { useNavigate } from "react-router-dom";
import { IoArrowBackCircle } from "react-icons/io5";
import { useState } from "react";
import { postAPI } from "../Services/apiService";
import InputComponent from "../Components/InputComponent";
import SelectComponent from "../Components/SelectComponent";
import Button from "../Components/Button";

const AddAPIPage = () => {
  const navigate = useNavigate();
  const navigateHome = () => navigate("/");

  // const { addApiToContext } = useApiContext();

  const [api, setApi] = useState({
    name: "",
    endpoint: "",
    description: "",
    status: "NEW",
  });

  const statusOptions = ["NEW", "FREE", "UNCERTAIN", "UNAVAILABLE"];

  function handleChange(event) {
    const { name, value } = event.target;
    setApi({ ...api, [name]: value });
  }

  function handleSubmit(event) {
    event.preventDefault();

    postAPI(api).then(() => {
      navigateHome();
    });
  }

  return (
    <>
      <div className="flex items-center p-5">
        <IoArrowBackCircle className="size-8 text-gray-900 cursor-pointer mr-2" onClick={navigateHome} />
        <h1 className=" h-full text-xl font-bold">Add API</h1>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="px-5 md:w-1/2 w-full">
          <InputComponent
            type="text"
            id="name"
            name="name"
            placeholder="API name"
            value={api.name}
            handleChange={handleChange}
          />
          <InputComponent
            type="url"
            id="endpoint"
            name="endpoint"
            placeholder="API endpoint"
            value={api.endpoint}
            handleChange={handleChange}
          />
          <InputComponent
            type="text"
            id="description"
            name="description"
            placeholder="API description"
            value={api.description}
            handleChange={handleChange}
          />

          <SelectComponent id="APIStatus" name="status" handleChange={handleChange} statusOptions={statusOptions} />

          <Button type="submit" text="Add API" classList="bg-blue-600 text-white px-4 py-2 rounded ml-auto" />
        </div>
      </form>
    </>
  );
};

export default AddAPIPage;
