import { useNavigate } from "react-router-dom";
import ApiList from "../Components/ApiList";

import useAuth from "../Context/AuthProvider";
import { useEffect } from "react";

const HomePage = () => {
  const { state } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    if (!state.isAuthenticated) {
      navigate("/login");
    }
  }, []);

  return (
    <>
      <ApiList />
    </>
  );
};

export default HomePage;
