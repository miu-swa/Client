import { useEffect, useState } from "react";
import ServiceComponent from "../Components/ServiceComponent";
import { getRunningServices } from "../Services/RunningService";

export default function RunningServices() {
  const [services, setServices] = useState([]);

  useEffect(() => {
    getRunningServices().then(data => {
      setServices(data);
    });
  }, []);

  return (
    <>
      <div className="flex items-center">
        <h1 className="p-5 h-full text-xl font-bold">All Running Service</h1>
      </div>

      {services.map(service => {
        return <ServiceComponent key={service.id} service={service} />;
      })}
    </>
  );
}
