import { useNavigate } from "react-router-dom";
import { IoArrowBackCircle } from "react-icons/io5";
import { useState } from "react";
import { postAPI } from "../Services/apiService";
import InputComponent from "../Components/InputComponent";
import SelectComponent from "../Components/SelectComponent";
import Button from "../Components/Button";
import { postCrawler } from "../Services/CrawlerService";

const AddCrawlerPage = () => {
  const navigate = useNavigate();

  // const { addApiToContext } = useApiContext();

  const [url, setUrl] = useState("");

  function handleSubmit(event) {
    event.preventDefault();
    const urls = url.split(",").map(url => url.trim());

    postCrawler(urls).then(res => {
      navigate("/crawlers");
    });
  }

  return (
    <>
      <div className="flex items-center p-5">
        <IoArrowBackCircle className="size-8 text-gray-900 cursor-pointer mr-2" onClick={() => navigate("/crawlers")} />
        <h1 className=" h-full text-xl font-bold">Add Crawler Urls</h1>
        <small className="p-1.5 mt-2 text-gray-500">Comma Seperated List</small>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="px-5 md:w-1/2 w-full">
          <InputComponent
            type="text"
            id="url"
            name="url"
            placeholder="Url"
            value={url}
            handleChange={e => setUrl(e.target.value)}
          />

          <Button type="submit" text="Add" classList="bg-blue-600 text-white px-4 py-2 rounded ml-auto" />
        </div>
      </form>
    </>
  );
};

export default AddCrawlerPage;
