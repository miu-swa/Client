import { useState, useEffect } from "react";
import CrawlerListComponent from "../Components/CrawlerListComponent";
import { Link } from "react-router-dom";
import { IoMdAddCircleOutline } from "react-icons/io";
import { fetchCrawlerData } from "../Services/CrawlerService";

export default function CrawlerPage() {
  const [urls, seturls] = useState([]);

  useEffect(() => {
    fetchCrawlerData().then(data => {
      seturls(data);
    });
  }, []);

  const handleDelete = url => {
    seturls(urls.filter(item => item !== url));
  };

  return (
    <>
      <div className="flex items-center">
        <h1 className="p-5 h-full text-xl font-bold">Crawlers Url</h1>
        <Link to={"new"} className="ml-auto bg-gray-900 text-white p-2  rounded  flex items-center mr-4">
          <IoMdAddCircleOutline className="size-5 text-white mx-1" />
        </Link>
      </div>

      {urls.map((url, index) => {
        return <CrawlerListComponent key={url + index} url={url} handleDelete={handleDelete} />;
      })}
    </>
  );
}
