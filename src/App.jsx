import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";

import HomePage from "./Pages/HomePage";
import AddAPIPage from "./Pages/AddAPIPage";
import NotFound from "./Pages/NotFound";
import Nav from "./Components/Nav";
import RunningServices from "./Pages/RunningServices";
import CrawlerPage from "./Pages/CrawlerPage";
import AddCrawlerPage from "./Pages/AddCrawlerPage";
import LoginPage from "./Pages/LoginPage";
import { AuthProvider } from "./Context/AuthProvider";
// import { ApiProvider } from "./context/ApiContext";

function App() {
  document.body.classList.add("bg-gray-100");

  return (
    <div className="App">
      {/* <ApiProvider> */}
      <AuthProvider>
        <BrowserRouter>
          <Nav />
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/add" element={<AddAPIPage />} />
            <Route path="/services" element={<RunningServices />} />
            <Route path="/crawlers" element={<CrawlerPage />} />
            <Route path="/crawlers/new" element={<AddCrawlerPage />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </BrowserRouter>
      </AuthProvider>
      {/* </ApiProvider> */}
    </div>
  );
}

export default App;
