export default function InputComponent({ type, id, name, placeholder, value, handleChange }) {
  return (
    <>
      <label htmlFor={id} className="block my-2 text-sm font-medium text-gray-900 ">
        {placeholder}
      </label>
      <input
        type={type}
        id={id}
        name={name}
        value={value}
        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
        placeholder={placeholder}
        onChange={handleChange}
        required></input>
    </>
  );
}
