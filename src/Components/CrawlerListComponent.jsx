import { deleteCrawler } from "../Services/CrawlerService";

const CrawlerListComponent = ({ url, handleDelete }) => {
  const deleteUrl = async url => {
    deleteCrawler(url).then(() => {
      handleDelete(url);
    });
  };

  return (
    <div className="bg-white p-4 mb-4 rounded flex mx-4 shadow justify-between border-t-4 border-solid border-green-400">
      <div className="flex flex-col items-start">
        <a href={url} className="text-sm text-blue-600 mb-2">
          {url}
        </a>
      </div>
      <button className="bg-red-600 text-center text-sm text-white px-4 py-2 rounded" onClick={() => deleteUrl(url)}>
        Delete
      </button>
    </div>
  );
};

export default CrawlerListComponent;
