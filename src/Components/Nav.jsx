import { FaSpider } from "react-icons/fa";
import { Link } from "react-router-dom";

const Nav = () => {
  return (
    <nav className="flex items-center px-4 bg-gray-900">
      <Link to={"/"} className="flex items-center">
        <div className="bg-white rounded-full p-2 ">
          <FaSpider className="size-8 text-gray-900 " />
        </div>
        <h1 className="p-5 h-full text-2xl font-bold text-white">Ankaboot</h1>
      </Link>

      <div className="ml-auto">
        <Link to={"/"} className="ml-auto p-2 text-white px-4 rounded mx-4">
          APIs
        </Link>
        <Link to={"/services"} className="ml-auto p-2 text-white px-4 rounded mx-4">
          Running Services
        </Link>
        <Link to={"/crawlers"} className="ml-auto p-2 text-white px-4 rounded mx-4">
          Crawler List
        </Link>
        {/* <Link to={"/reports"} className="ml-auto p-2 text-white px-4 rounded mx-4">
          Reports
        </Link> */}
      </div>
    </nav>
  );
};

export default Nav;
