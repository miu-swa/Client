import { useEffect } from "react";
import { FaDocker } from "react-icons/fa6";

const ServiceComponent = ({ service }) => {
  useEffect(() => {
    console.log(service);
  }, [service]);

  return (
    <div className="bg-white p-4 mb-4 rounded flex mx-4 shadow items-end border-t-4 border-solid border-blue-400">
      <div className="flex flex-col items-start">
        <p className="text-lg font-semibold mb-2">
          {service.id} - {service.image} - {service.containerName}
        </p>
        <a className="text-sm">Port: {service.port}</a>
      </div>
      <FaDocker className="ml-auto size-8 text-blue-500" />
    </div>
  );
};

export default ServiceComponent;
