FROM node:18-alpine

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install

COPY . .

ENV VITE_API_BASE_URL = http://localhost:8080/api
ENV VITE_API_CRAWLER_URL = http://localhost:8080/crawler/urls
ENV VITE_API_SERVICES_URL = http://localhost:8080/services/running
ENV PORT = 3000

EXPOSE 8080

CMD [ "yarn", "dev" ]