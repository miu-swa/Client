module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:react/jsx-runtime",
    "plugin:react-hooks/recommended",
  ],
  ignorePatterns: ["dist", ".eslintrc.cjs"],
  parserOptions: { ecmaVersion: "latest", sourceType: "module" },
  settings: { react: { version: "18.2" } },
  plugins: ["react-refresh"],

  rules: {
    "react/react-in-jsx-scope": "off",
    "react/jsx-no-target-blank": "off",
    "react/no-unescaped-entities": [
      "error",
      {
        forbid: [
          {
            char: ">",
            alternatives: ["&gt;"],
          },
          {
            char: "}",
            alternatives: ["&#125;"],
          },
        ],
      },
    ],
    "array-callback-return": "warn",
    "react/prop-types": "off",
    "no-debugger": "warn",
    quotes: ["warn", "double"],
    semi: ["error", "always"],
    "no-var": "warn",
    // "@typescript-eslint/no-unused-vars": "warn",
    "no-unused-vars": "warn",
    "@typescript-eslint/no-empty-interface": "off",
    "react-refresh/only-export-components": ["warn", { allowConstantExport: true }],
  },
};
